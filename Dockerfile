FROM odoo:12.0
LABEL MAINTAINER Daniel Moreno <danielvdmlfiis@gmail.com>
USER root

COPY ./requirements.txt /etc/
RUN python3 -m pip install -r /etc/requirements.txt
# RUN python3 -m pip install jtypes.py4j